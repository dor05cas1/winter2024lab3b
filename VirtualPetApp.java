import java.util.Scanner;

public class VirtualPetApp{

	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		
		
		Mammoth [] herdmammoths = new Mammoth [4];
		for(int i = 0; i < herdmammoths.length; i++){
			herdmammoths[i] = new Mammoth();
			System.out.println("Enter the type of the mammoth: ");
			herdmammoths[i].type = reader.nextLine();
			System.out.println("Enter the diet of the mammoth: ");
			herdmammoths[i].diet = reader.nextLine();
			System.out.println("Enter the height of the mammoth: ");
			herdmammoths[i].height = Integer.parseInt(reader.nextLine());
		}
		
		
		System.out.println(herdmammoths[3].type);
		System.out.println(herdmammoths[3].diet);
		System.out.println(herdmammoths[3].height);
		
		herdmammoths[0].eatFood();
		herdmammoths[0].rankMammoth();
	}
}