public class Mammoth{
	public String diet;
	public String type;
	public int height;
	
	public void eatFood(){
		if(this.diet.equals("Plants")){
			System.out.println("Yum I am eating plants");
		}
		else if(this.diet.equals("Forbs")){
			System.out.println("Yum I am eating forbs");
		}
		else{
			System.out.println("I am eating grasses");
		}
	}
	public void rankMammoth(){
		if(this.height > 4){
			System.out.println("Yum I am one of the tallest types of mammoths");
		}
		else if(this.height == 1){
			System.out.println("I am the smallest mammoth type");
		}
		else{
			System.out.println("I am a medium sized mammoth");
		}
	}
}